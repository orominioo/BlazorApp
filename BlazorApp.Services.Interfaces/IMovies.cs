﻿using BlazorApp.Models;
using System.Collections.Generic;

namespace BlazorApp.Services.Interfaces
{
    public interface IMovies
    {
        List<Movie> GetMovies();
        Movie GetMovieById(int id);
    }
}